CC= gcc
OBJECTS= ./build/compiler.o ./build/cprocess.o ./build/lexer.o ./build/token.o ./build/lex_process.o ./build/parser.o ./build/node.o ./build/helpers/buffer.o ./build/helpers/vector.o
INCLUDES= -I./

$(shell mkdir -p ./build/helpers)

all: ${OBJECTS}
	${CC} main.c ${INCLUDES} ${OBJECTS} -g -o ./main

./build/compiler.o: ./compiler.c
	${CC} ./compiler.c ${INCLUDES} -o ./build/compiler.o -g -c

./build/cprocess.o: ./compiler.c
	${CC} ./cprocess.c ${INCLUDES} -o ./build/cprocess.o -g -c

./build/lexer.o: ./compiler.c
	${CC} ./lexer.c ${INCLUDES} -o ./build/lexer.o -g -c

./build/token.o: ./compiler.c
	${CC} ./token.c ${INCLUDES} -o ./build/token.o -g -c

./build/lex_process.o: ./compiler.c
	${CC} ./lex_process.c ${INCLUDES} -o ./build/lex_process.o -g -c

./build/parser.o: ./parser.c
	${CC} ./parser.c ${INCLUDES} -o ./build/parser.o -g -c

./build/node.o: ./node.c
	${CC} ./node.c ${INCLUDES} -o ./build/node.o -g -c

./build/helpers/buffer.o: ./helpers/buffer.c
	${CC} ./helpers/buffer.c ${INCLUDES} -o ./build/helpers/buffer.o -g -c

./build/helpers/vector.o: ./helpers/vector.c
	${CC} ./helpers/vector.c ${INCLUDES} -o ./build/helpers/vector.o -g -c

clean:
	rm -rf ${OBJECTS}
	rm ./main
